<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 7:55 PM
 * @Updated: 3/10/2019 7:55 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\Setting;


use Illuminate\Support\ServiceProvider;

/**
 * Class SettingServiceProvider
 *
 * @package Natenju\Setting
 */
class SettingServiceProvider extends ServiceProvider {
    
    protected $policies = [];
    
    /**
     * Register services.
     *
     * @return void
     */
    public function register() { }
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot() { }
}

<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 7:58 PM
 * @Updated: 3/10/2019 7:58 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\Setting\Facades;


use Illuminate\Support\Facades\Facade;

class Setting extends Facade {
    protected static function getFacadeAccessor() {
        return 'setting';
    }
}
<?php
/**
 * @Author Tchapga Nana Hermas <[hermastchapga@gmail.com]>.
 * @Created: 3/10/2019 8:08 PM
 * @Updated: 3/10/2019 8:08 PM
 * @Desc   : [DESCRIPTION]
 */

namespace Natenju\Setting\App;


use Illuminate\Database\Eloquent\Model;

class Setting extends Model {
    protected $table      = 'settings';
    protected $guarded    = [];
    public    $timestamps = FALSE;
}